﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static GameManager instance;
    public GameObject player;

    private int score;
    private int lives;
    private int highscore;
    private Vector2 savePoint;

    public Text scoreText;
    public Text livesText;
    public Text highscoreText;

    // Use this for initialization
    private void Awake () {
        // Setup the singleton
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {
        highscore = PlayerPrefs.GetInt("highscore", 0);
        savePoint = player.transform.position;
        
        BeginGame();
    }


    void BeginGame()
    {
        score = 0;
        lives = 3;

        scoreText.text = "SCORE: " + score;
        highscoreText.text = "HISCORE: " + highscore;
        livesText.text = "LIVES: " + lives;


    }

    public void DecrementLives()
    {
        lives--;
        livesText.text = "LIVES: " + lives;

        // Has player run out of lives?
        if (lives < 1)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }

        Respawn();
    }
    void Respawn()
    {
        player.transform.position = savePoint;
    }

    public void SavePointLocation()
    {
        savePoint = player.transform.position;
    }

    public void Win()
    {
        SceneManager.LoadScene(2);
    }

    public void IncrementScore()
    {
        score++;
        scoreText.text = "SCORE:" + score;

        if (score > highscore)
        {
            highscore = score;
            highscoreText.text = "HISCORE: " + highscore;

            // Save the new hiscore
            PlayerPrefs.SetInt("highscore", highscore);
        }
    }
}
