﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavePoints : MonoBehaviour {
    private GameManager gameController;


    private void Start()
    {
        

        GameObject gameControllerObject =
              GameObject.FindWithTag("GameController");

        gameController =
            gameControllerObject.GetComponent<GameManager>();
    }


    void OnCollisionEnter2D(Collision2D colision)
    {
        if (colision.gameObject.tag.Equals("Player"))
        {
            
            // Destroy coin
            Destroy(gameObject);
            // Add to the score
            gameController.SavePointLocation();
        }

    }
}
