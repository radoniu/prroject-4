﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpingEnemy : MonoBehaviour {
    public Transform ray;
    public float distance = 1f;
    public float moveSpeed = 10f;
    private GameManager gameController;
    private bool movingUp;

    // Use this for initialization
    void Awake()
    {
        GameObject gameControllerObject =
         GameObject.FindWithTag("GameController");

        gameController =
            gameControllerObject.GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {

        transform.Translate(Vector2.up * moveSpeed * Time.deltaTime);
        RaycastHit2D hitInfo = Physics2D.Raycast(ray.position, Vector2.up, distance);

        if (hitInfo.collider == true)
        {
            if (movingUp == true)
            {
                transform.eulerAngles = new Vector2(-180, 0);
                movingUp = false;
            }
            else
            {
                transform.eulerAngles = new Vector2(0, 0);
                movingUp = true;
            }

        }
    }
    void OnCollisionEnter2D(Collision2D colision)
    {
        if (colision.gameObject.tag.Equals("Player"))
        {


            gameController.DecrementLives();
        }

    }

}
