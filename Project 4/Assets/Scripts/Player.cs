﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {

    public Transform tf;
    public float moveSpeed = 10f;
    public float jumpForce = 10f;
    private Rigidbody2D rg;
    private bool onGround = true;

    private GameManager gm;

    void Start()
    {
        tf = gameObject.transform;
        rg = GetComponent<Rigidbody2D>();

        GameObject gameControllerObject =
            GameObject.FindWithTag("GameController");

        gm = gameControllerObject.GetComponent<GameManager>();
    }

    private void FixedUpdate()
    {
        
        if (Input.GetKeyDown(KeyCode.UpArrow))
		{
			Jump();
        }

		if (Input.GetKey(KeyCode.LeftArrow))
		{
			Move(-tf.right);
		}

		if (Input.GetKey(KeyCode.RightArrow))
		{
			Move(tf.right);
		}
        if (Input.GetKey(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }
    }

	public void Move(Vector3 direction)
    {
        // Move in the direction passed in, at speed "moveSpeed"
        tf.position += (direction.normalized * moveSpeed * Time.deltaTime);


    }
    public void Jump()
    {
        rg.velocity = Vector2.up * jumpForce;

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Equals("Enemy"))
        {
            gm.DecrementLives();
            
        }
        if (collision.gameObject.tag.Equals("Goal"))
        {
            gm.Win();

        }
    }
}

