﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingEnemy : MonoBehaviour {
    public Transform ray;
    public float distance = 1f;
    public float moveSpeed = 10f;

    private Rigidbody2D rg;

    private bool movingRight;
    private GameManager gameController;
    // Use this for initialization
    void Awake () {
        
        GameObject gameControllerObject =
         GameObject.FindWithTag("GameController");

        gameController =
            gameControllerObject.GetComponent<GameManager>();
    }
	
	// Update is called once per frame
	void Update () {
       
        transform.Translate(Vector2.right * moveSpeed * Time.deltaTime);
        RaycastHit2D hitInfo = Physics2D.Raycast(ray.position, Vector2.right, distance);

        if (hitInfo.collider == true)
        {
            if(movingRight == true)
            {
                transform.eulerAngles = new Vector2(0, -180);
                movingRight = false;
            }
            else
            {
                transform.eulerAngles = new Vector2(0, 0);
                movingRight = true;
            }

        }
    }
    void OnCollisionEnter2D(Collision2D colision)
    {
        if (colision.gameObject.tag.Equals("Player"))
        {
           
          
            gameController.DecrementLives();
        }

    }
}
